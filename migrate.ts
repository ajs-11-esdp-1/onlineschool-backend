import sequelize from './src/config/db.config';

const runMigrations = async () => {
    try {
        await sequelize.authenticate();
        await sequelize.sync({ force: true }); // Установите force: true, чтобы пересоздать таблицы
        console.log('Migrations completed successfully.');
    } catch (error) {
        console.error('Migrations failed:', error);
    } finally {
        await sequelize.close();
    }
};

runMigrations();