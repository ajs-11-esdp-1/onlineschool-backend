# Stage 1: Build the app
FROM node:20-alpine AS build

WORKDIR /app

COPY package*.json ./

RUN npm install -g npm@latest && npm install

RUN npm cache clean --force

COPY . .

RUN npm run build


# Stage 2: Run the app
FROM node:20-alpine

WORKDIR /app

COPY --from=build /app/dist /app/dist
COPY --from=build /app/node_modules /app/node_modules
COPY --from=build /app/package.json /app/package.json

EXPOSE 8000

CMD ["npm", "start"]
