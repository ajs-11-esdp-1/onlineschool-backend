import jwt from 'jsonwebtoken';

const generateAccessToken = (email: string) => {
  const payload = {
    email,
  };
  return jwt.sign(payload, process.env.SECRET || 'secret_KEY', {
    expiresIn: '24h'});
};

export default generateAccessToken;