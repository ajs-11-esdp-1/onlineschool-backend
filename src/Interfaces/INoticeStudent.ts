import { NoticeType } from '../config/enums/NoticeType';
import { Model } from 'sequelize';

export default interface INotice extends Model {
    sender : number
    recipient: number
    model_id: number 
    notice_type: NoticeType
    message: string
    isRead: boolean
}

export interface INoticeStudent {
    sender : number
    recipient: number[]
    model_id: number 
    notice_type: NoticeType
    message: string
    isRead: boolean
}