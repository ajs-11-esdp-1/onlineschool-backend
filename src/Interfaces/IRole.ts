import {Model} from 'sequelize';

export interface IRole extends Model {
    id: number;
    role: string;
}