import { Model } from "sequelize";

export interface IDirection extends Model {
    id :number
    direction :string
}