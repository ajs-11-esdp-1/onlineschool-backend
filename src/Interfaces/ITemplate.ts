import { Model } from "sequelize"

export interface ITemplate extends Model {
    id:number
    title: string
    description:string
}