import { Model } from "sequelize"

export interface ITeacherStudent extends Model {
    teacher_id:number
    student_id:number
}