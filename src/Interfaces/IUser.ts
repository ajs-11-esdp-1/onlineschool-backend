import {Model} from 'sequelize';

export interface IUser extends Model {
    id: number;
    userName: string;
    firstName: string;
    lastName: string;
    password: string;
    email: string;
    phone: string;
    avatar: string;
    token: string;
    role_id: number;
    isConfirmed :boolean
    checkPassword: (password: string) => Promise<boolean>;
    generateToken: () => void;
    setAvatar: (avatar: string) => void;
    toJSON: () => Omit<IUser , 'password'>;
}