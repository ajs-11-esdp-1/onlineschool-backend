import {Model} from 'sequelize';

export interface ISchool extends Model {
    id: number;
    schoolName: string;
    password: string;
    email: string;
    phone: number;
    avatar: string;
    token: string;
    role_id: number;
    checkPassword: (password: string) => boolean;
    generateToken: () => void;
    setAvatar: (avatar: string) => void;
    toJSON: () => Omit<ISchool , 'password'>;
}