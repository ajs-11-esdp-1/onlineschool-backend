import { Model } from "sequelize";

export interface IPictureTeacher extends Model {
    lesson_id: number
    teacher_id: number
    picture: string
}


