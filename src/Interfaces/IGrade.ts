import { Model } from "sequelize";

export default interface IGrade extends Model {
    id:number
    lesson_id:number
	grade:string
	transit_time: string
	readed: boolean
	student_id:number
    lesson_answer:JSON
    teacher_id:number
    review: string
}

