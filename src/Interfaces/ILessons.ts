import { Model } from "sequelize";

export interface ILessons extends Model {
    id: number 
    title: string
    description:string
    lesson:JSON
    transit_time:string
    template_id:number | null
    teacher_id:number
    direction_id:number
	pay_order: number | null
    public:boolean
}