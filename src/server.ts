import express , {Express} from 'express';
import sequelize from './config/db.config';
import cors from 'cors';
import authController from './controllers/auth.controller';
import roleController from './controllers/role.controller';
import templateController from './controllers/template.controllers';
import LessonController from './controllers/lesson.controller';
import directionController from './controllers/direction.controller';
import GradeController from './controllers/grade.controller';
import noticeController from './controllers/notice.controller';
import teacherStudentController from './controllers/teacher.student.controller';

const app:Express = express();
const PORT        = 8000;


const run = async() => {

  try {
    await sequelize.authenticate();
    await sequelize.sync();
    app.listen(PORT , () => console.log( 'Server start on ' , PORT));

  } catch (error) {
    console.log(error);
  }
};


app.use(express.static('/api/public'));
app.use(cors());
app.use(express.json());

app.use('/api/auth' , authController);
app.use('/api/roles', roleController);
app.use('/api/template' , templateController);
app.use('/api/lesson' , LessonController);
app.use('/api/direction' , directionController);
app.use('/api/grades' , GradeController);
app.use('/api/teacher' , teacherStudentController);
app.use('/api/notice' , noticeController);

run().catch(e => console.log(e));