import UserModel from "../models/User.model";
import GradeDto from "../dto/Grade.dto";
import GradeModel from "../models/Grade.model";
import {Router, Request, Response} from "express";
import authMiddleware from "../middlewares/auth.middleware";
import IGrade from "../Interfaces/IGrade";
import {IRequest} from "../Interfaces/IRequest";
import {NoticeModel} from "../models/Notice";
import {NoticeTeacherByGradeDto} from "../dto/Notice.dto/Notice.teacher.dto";
import {TeacherStudentModel} from "../models/TeacherStudent.model";


const GradeController: Router = Router()

GradeController.post('/save', authMiddleware, async (req: Request, res: Response) => {

    const {
        lesson_id,
        grade,
        transit_time,
        student_id,
        lesson_answer,
        teacher_id,
    } = req.body as Omit<IGrade, 'id' | 'readed' | 'review'>


    const copy: boolean = [grade, transit_time].map((val: string) => {

        return val.trim() !== '' ? false : true

    }).includes(true)

    try {

        const studentByTeacher = await TeacherStudentModel.findOne({
            where: {
                student_id: student_id,
                teacher_id: teacher_id
            }
        })

        if (studentByTeacher && !copy) {

            const gradeDto: GradeDto = new GradeDto(
                lesson_id,
                grade,
                transit_time,
                student_id,
                teacher_id,
                lesson_answer
            )

            const response = await GradeModel.create({...gradeDto, lesson_answer: JSON.stringify(lesson_answer)})

            const noticeDto = new NoticeTeacherByGradeDto(studentByTeacher.student_id, studentByTeacher.teacher_id, response.id)

            const responseNotice = await NoticeModel.create({...noticeDto})

            await responseNotice.save()

            return res.send({...response, teacher: studentByTeacher.teacher_id, student: studentByTeacher.student_id})

        }


        throw new Error('Ошибка с полями не все заполнены')

    } catch (error) {
        return res.status(404).send(error)
    }


})

GradeController.get('/public/all', authMiddleware, async (req: Request, res: Response) => {

    try {

        const response = await GradeModel.findAll({
            include: [
                {
                    model: UserModel, as: 'teacher',
                    attributes: {exclude: ['password', 'token', 'isConfirmed', 'phone', 'email']}
                },
                {
                    model: UserModel, as: 'student',
                    attributes: {exclude: ['password', 'token', 'isConfirmed', 'phone', 'email']}
                },
                'lesson'
            ]
        })

        return res.send(response)

    } catch (error) {

        res.status(404).send('FAK')

    }

})

GradeController.get('/public/all/student', authMiddleware, async (req: IRequest, res: Response) => {

    const {user} = req

    try {

        const response = await GradeModel.findAll({
            where: {student_id: user.id},
            include: [
                {
                    model: UserModel, as: 'teacher',
                    attributes: {exclude: ['password', 'token', 'isConfirmed', 'phone', 'email']}
                },
                {
                    model: UserModel, as: 'student',
                    attributes: {exclude: ['password', 'token', 'isConfirmed', 'phone', 'email']}
                },
                'lesson'
            ]
        })

        res.send(response)

    } catch (error) {

        res.status(404).send('FAK')

    }

})

GradeController.get('/public/all/teacher', authMiddleware, async (req: IRequest, res: Response) => {

    try {

        const arrStudent = await TeacherStudentModel.findAll({where: {teacher_id: req.user.id}})

        if (arrStudent) {


            const response = await Promise.all(arrStudent.map(async (val) => {

                const grade = await GradeModel.findAll({
                    where: {student_id: val.student_id},
                    include: [
                        {
                            model: UserModel, as: 'teacher',
                            attributes: {exclude: ['password', 'token', 'isConfirmed', 'phone', 'email']}
                        },
                        {
                            model: UserModel, as: 'student',
                            attributes: {exclude: ['password', 'token', 'isConfirmed', 'phone', 'email']}
                        },
                        'lesson'
                    ]
                })


                return grade
            }))


            res.send(...response)

        }


    } catch (error) {

        res.status(404).send('FAK')

    }

})

GradeController.get('/public/readed/:id', authMiddleware, async (req: Request, res: Response) => {

    const {id} = req.params

    try {
        if (id) {
            const response = await GradeModel.findByPk(id, {
                include: [
                    {
                        model: UserModel, as: 'teacher',
                        attributes: {exclude: ['password', 'token', 'isConfirmed', 'phone', 'email']}
                    },
                    {
                        model: UserModel, as: 'student',
                        attributes: {exclude: ['password', 'token', 'isConfirmed', 'phone', 'email']}
                    },
                    'lesson'
                ]
            })

            if (response && response.readed) {

                response.readed = false

                await response.save()

                return res.send(response)

            }

        }
    } catch (error) {
        res.status(404).send('FAK')
    }

})

GradeController.get('/public/:id', authMiddleware, async (req: Request, res: Response) => {

    const {id} = req.params

    try {
        if (id) {
            const response = await GradeModel.findByPk(id, {
                include: [
                    {
                        model: UserModel, as: 'teacher',
                        attributes: {exclude: ['password', 'token', 'isConfirmed', 'phone', 'email']}
                    },
                    {
                        model: UserModel, as: 'student',
                        attributes: {exclude: ['password', 'token', 'isConfirmed', 'phone', 'email']}
                    },
                    'lesson'
                ]
            })

            return response && res.send(response)
        }
    } catch (error) {
        res.status(404).send('FAK')
    }

})

GradeController.post('/review/:grade_id', authMiddleware, async (req: IRequest, res: Response) => {

    const {grade_id} = req.params

    const {review} = req.body as Pick<GradeDto, 'review'>

    try {

        if (grade_id && review.trim() !== '') {

            const response = await GradeModel.findByPk(grade_id, {
                include: [
                    {
                        model: UserModel, as: 'teacher',
                        attributes: {exclude: ['password', 'token', 'isConfirmed', 'phone', 'email']}
                    },
                    {
                        model: UserModel, as: 'student',
                        attributes: {exclude: ['password', 'token', 'isConfirmed', 'phone', 'email']}
                    },
                    'lesson'
                ]
            })

            if (response) {

                const studentByTeacher = await TeacherStudentModel.findOne({
                    where: {
                        student_id: response.student_id,
                        teacher_id: req.user.id
                    }
                })

                if (studentByTeacher) {

                    const noticeDto = new NoticeTeacherByGradeDto(studentByTeacher.student_id, studentByTeacher.teacher_id, response.id)

                    const responseNotice = await NoticeModel.create({...noticeDto})

                    await responseNotice.save()

                    response.readed = true

                    response.review = review

                    await response.save()

                    return res.send(response)
                }

            }

        }
        res.status(404).send('FAK')

    } catch (error) {
        res.status(404).send('FAK')

    }

})


export default GradeController