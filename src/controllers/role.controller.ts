import {Router, Request, Response} from 'express';
import RoleDto from '../dto/Role.dto';
import RoleModel from '../models/Role.model';

const roleController: Router = Router();

roleController.post('/', async (req: Request, res: Response) => {
    try {

        const {role} = req.body as RoleDto;

        const roleDto: RoleDto = new RoleDto(role);

        const response = await RoleModel.create({...roleDto});

        await response.save();

        return res.send(response);

    } catch (error) {
        res.send(error);
    }
});

roleController.get('/', async (req: Request, res: Response) => {

    try {

        const response = await RoleModel.findAll()

        res.send(response)


    } catch (error) {

        res.status(404).send('FAK')

    }

})
export default roleController;