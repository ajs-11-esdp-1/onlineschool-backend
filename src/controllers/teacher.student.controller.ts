import {IRequest} from "../Interfaces/IRequest";
import TeacherStudentDto from "../dto/Teacher.student.dto";
import {UserRole} from "../helpers/enums/UserRole.enum";
import authMiddleware from "../middlewares/auth.middleware";
import {permitMiddleware} from "../middlewares/permit.middleware";
import {TeacherStudentModel} from "../models/TeacherStudent.model";
import UserModel from "../models/User.model";
import {Router, Response} from "express";


const teacherStudentController: Router = Router();


teacherStudentController.get('/by/student', authMiddleware, async (req: IRequest, res: Response) => {

    try {

        const response = await TeacherStudentModel.findAll({
                where: {teacher_id: req.user.id}, include: [{

                    model: UserModel,
                    as: 'student',
                    attributes: {exclude: ['password', 'token', 'isConfirmed', 'phone', 'email']},
                },
                ]
            },
        );

        res.send(response);

    } catch (error) {
        res.status(404).send('FAK');
    }
});

teacherStudentController.post('/add/student ', authMiddleware, permitMiddleware(UserRole.Teacher), async (req: IRequest, res: Response) => {

    try {

        const {student_id} = req.body as Pick<TeacherStudentDto, 'student_id'>;

        if (student_id) {


            const teacherStudentDto: TeacherStudentDto = new TeacherStudentDto(req.user.id, student_id);

            const response = await TeacherStudentModel.create({...teacherStudentDto});

            if (response) {

                response.save();

                res.send(response);
            }
        }
    } catch (error) {
        res.status(404).send('FAK');
    }
});


export default teacherStudentController;