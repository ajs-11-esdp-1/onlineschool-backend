import {Router, Request, Response} from 'express';
import UserModel from '../models/User.model';
import authMiddleware from '../middlewares/auth.middleware';
import {IRequest} from '../Interfaces/IRequest';
import {nanoid} from 'nanoid';
import RoleModel from '../models/Role.model';
import SchoolDto from '../dto/School.dto';
import SchoolModel from '../models/School.model';
import sendMail from '../config/email.config';
import {IMail} from '../Interfaces/IMail';
import userChangeDto from '../dto/User.dto/User.change.info';
import userAuthDto from '../dto/User.dto/User.auth';
import uploadMiddleware from '../middlewares/upload.middleware';
import {host} from "../constants/constants";


const authController: Router = Router();


authController.post('/register', async (req: Request, res: Response) => {

    const confirmationCode = nanoid();

    const data: IMail = {
        recipient: req.body.email,
        check: 'ESDP CHECK BRO',
        theme: 'Confirmation message',
        link: `${process.env.FRONT_URL || host}/confirm/${confirmationCode}`,

    };


    try {
        const roles = await RoleModel.findByPk(req.body.role_id);

        const EMAIL_REGEXP: RegExp = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu;


        if (!EMAIL_REGEXP.test(req.body.email)) {
            throw new Error('Не верный email');
        }

        if (roles) {
            if (roles.role === 'teacher') {

                const {password, firstName, lastName, phone, email} = req.body as userAuthDto;

                const userDto: userAuthDto = new userAuthDto(firstName, lastName, password, roles.id, phone, email, confirmationCode);

                const response = await UserModel.create({...userDto});

                await response.save();

                sendMail(data); // Данная функция отправляет письмо на почту пользователя с ссылкой
                // На фронте есть роут на http://localhost:3000/confirm/:email/:confirmCode


                return res.send(response);

            } else if (roles.role === 'school') {

                const {schoolName, password, phone, email} = req.body as SchoolDto;

                const schoolDto: SchoolDto = new SchoolDto(schoolName, password, roles.id, phone, email);

                const response = await SchoolModel.create({...schoolDto});

                await response.save();

                sendMail(data); // Данная функция отправляет письмо на почту пользователя с ссылкой
                // На фронте есть роут на http://localhost:3000/confirm/:email/:confirmCode


                return res.send(response);
            }
        }

        const student = await RoleModel.findOne({where: {role: 'student'}});

        if (student) {

            const {password, firstName, lastName, phone, email} = req.body as userAuthDto;

            const userDto: userAuthDto = new userAuthDto(firstName, lastName, password, student.id, phone, email, confirmationCode);

            const response = await UserModel.create({...userDto});

            await response.save();

            sendMail(data); // Данная функция отправляет письмо на почту пользователя с ссылкой
            // На фронте есть роут на http://localhost:3000/confirm/:email/:confirmCode


            return res.send(response);
        }

    } catch (error) {

        return res.status(500).json({error: 'An error occurred while registering.'});

    }
});


authController.get('/confirm', async (req: Request, res: Response) => {

    const {confirmationCode} = req.query;

    try {
        if (confirmationCode) {
            const user = await UserModel.findOne({where: {token: confirmationCode}});

            if (user && !user.isConfirmed) {


                // Здесь подтверждается почта пользователя
                //уточняется сущ. пользователь или нет и также поле для
                //подтверждения которое поумолчанию false

                user.isConfirmed = true;

                await user.save();

                return res.send('Registration confirmed. You can now log in.');

            } else {

                return res.status(400).send('Invalid email or confirmation code.');


            }
        }
    } catch (error) {

        return res.status(500).send('An error occurred while confirming registration.');

    }
});

authController.post('/login', async (req: Request, res: Response) => {
    try {

        const {email, password} = req.body as userAuthDto;

        const user = await UserModel.findOne(
            {
                where: {
                    email: email,
                },
                include: 'role'
            });


        const isMatch = await user?.checkPassword(password);

        if (user && isMatch && user.isConfirmed) {

            user.generateToken();

            await user.save();

            return res.send(user);


        } else {

            return res.status(401).json({error: 'Invalid email or password.'});

        }
    } catch (error) {

        return res.status(500).json({error: 'An error occurred while logging in.'});

    }
});

authController.delete('/logout', authMiddleware, async (req: IRequest, res: Response) => {

    req.user.generateToken();

    await req.user.save();

    return res.send({message: 'Logout success'});

});

authController.put('/edit', authMiddleware, uploadMiddleware.single('avatar'), async (req: IRequest, res: Response) => {
    const {
        firstName,
        lastName,
        phone,
    } = req.body as userChangeDto;

    let avatar: string = req.user.avatar;

    if (req.file) avatar = req.file.filename;

    const response: userChangeDto = new userChangeDto(firstName, lastName, phone, avatar);

    req.user.firstName = response.firstName;
    req.user.lastName = response.lastName;
    req.user.phone = response.phone;

    await req.user.save();

    res.send(req.user);
});

authController.get('/:id', async (req: Request, res: Response) => {

    const {id} = req.params;

    if (id) {

        const users = await UserModel.findOne({where: {token: id}});

        if (users) {

            return res.send(users);

        }
    }

    return res.status(404).send('Error');

});

authController.post('/change_pass', (req: Request, res: Response) => {

    // const {username , password , newpass} = req.body;

});

authController.get('/', async (req: Request, res: Response) => {

    const users = await UserModel.findAll();

    return res.send(users);
});

export default authController;