import LessonsDto from "../dto/Lesson.dto";
import LessonsModel from "../models/Lessons.model";
import {Router, Request, Response} from "express";
import multer from "multer";
import {uploadPictureByTutorial} from "../../config";
import UserModel from "../models/User.model";
import TemplateModel from "../models/Template.model";
import DirectionsModel from "../models/Directions.model";
import {IRequest} from "../Interfaces/IRequest";
import authMiddleware from "../middlewares/auth.middleware";
import {IPictureTeacher} from "../Interfaces/PictureTeacher";
import PictureTeacherModel from "../models/Picture.teacher.model";
import {PictureTutorialFS} from "../fileSystem";
import { UserRole } from "../helpers/enums/UserRole.enum";
import {permitMiddleware} from "../middlewares/permit.middleware";

const storage = multer.diskStorage({
    destination: (req, file, cb) => {

        cb(null, uploadPictureByTutorial);

    },
    filename: (req, file, cb) => {

        cb(null, file.originalname);

    },
});

const upload = multer({storage});

const LessonController: Router = Router();


LessonController.get('/', async (req: Request, res: Response) => {

    try {
        const response = await LessonsModel.findAll({include: 'template'});

        res.send(response);
    } catch (error) {
        res.status(404).send(error);
    }

});

LessonController.get('/by/teacher', authMiddleware, async (req: IRequest, res: Response) => {

    const {user} = req;

    try {

        const response = await LessonsModel.findAll({where: {teacher_id: user.id}, include: ['template', 'direction']});

        res.send(response);
    } catch (error) {
        res.status(404).send(error);
    }

});


LessonController.get('/public', async (req: Request, res: Response) => {

    try {
        const response = await LessonsModel.findAll({
            where: {public: true},
            include: ['template', 'user', 'direction']
        });

        res.send(response);


    } catch (error) {
        res.status(404).send(error);
    }

});

LessonController.post('/save', async (req: Request, res: Response) => {

    const {
        title,
        description,
        lesson,
        transit_time,
        template_id,
        teacher_id,
        direction_id,
    } = req.body as LessonsDto;


    try {
        const copy: boolean = [title, description, transit_time]
            .map((val: string) => {

                return val.trim() !== '' ? false : true;

            }).includes(true);


        if (!copy) {

            const tutorial = new LessonsDto(
                title,
                description,
                lesson,
                transit_time,
                template_id,
                teacher_id,
                direction_id,
            );

            const template = await TemplateModel.findByPk(tutorial.template_id);

            const teacher = await UserModel.findByPk(tutorial.teacher_id);

            const direction = await DirectionsModel.findByPk(tutorial.direction_id);

            if (template && teacher && direction) {


                const response = await LessonsModel.create({
                    ...tutorial,
                    lesson: JSON.parse(JSON.stringify(tutorial.lesson))
                });

                await response.save();

                return res.send(response);
            }


        }

    } catch (error) {

        res.status(500).send(error);

    }


});
LessonController.post('/save/picture/:lesson_id', upload.array('picture'), authMiddleware, async (req: IRequest, res: Response) => {

    try {
        const {lesson_id} = req.params;

        let arrPicture: Pick<IPictureTeacher, 'lesson_id' | 'picture' | 'teacher_id'> [] = [];

        if (req.files && Array.isArray(req.files) && lesson_id) {

            req.files.map((val) => {
                arrPicture.push({
                    lesson_id: Number(lesson_id),
                    teacher_id: req.user.id,
                    picture: val.originalname,
                });
            });
            const response = await PictureTeacherModel.bulkCreate(arrPicture);
        }
        res.send('');
    } catch (error) {
        res.status(404).send(error);
    }

});

LessonController.put('/published/:id', authMiddleware, async (req: IRequest, res: Response) => {

    try {

        const {id} = req.params;

        const response = await LessonsModel.findOne({where: {id: id, teacher_id: req.user.id}});

        if (response) {

            response.public = true;

            await response.save();

            res.send(response);

        }


    } catch (error) {
        res.status(404).send(error);
    }

});

LessonController.delete('/delete/:lesson_id', authMiddleware, permitMiddleware(UserRole.Teacher), async (req: IRequest, res: Response) => {

    try {
        const {lesson_id} = req.params;

        const response = await LessonsModel.destroy({where: {id: lesson_id, teacher_id: req.user.id}});

        const pictureTeachers = await PictureTeacherModel.findAll({where: {id: lesson_id, teacher_id: req.user.id}});

        if (pictureTeachers) {

            const arrPicture: string [] = pictureTeachers.map((val) => {
                return val.picture;
            });

            PictureTutorialFS.deleteArrPicture(arrPicture);
        }

        res.send('Туториал удален');

    } catch (error) {
        res.status(404).send(error);

    }

});

export default LessonController;