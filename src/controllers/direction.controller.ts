import DirectionsModel from "../models/Directions.model";
import {Router, Response, Request} from "express";

const directionController: Router = Router()

directionController.get('/', async (req: Request, res: Response) => {

    try {

        const response = await DirectionsModel.findAll()

        res.send(response)

    } catch (error) {
        res.status(404).send("FAK")
    }

})


export default directionController