import {Router , Response } from 'express'
import authMiddleware from '../middlewares/auth.middleware'
import { IRequest } from '../Interfaces/IRequest'
import { NoticeModel } from '../models/Notice'
import NoticeStudentDto from '../dto/Notice.dto/Notice.student.dto'
import INotice, {INoticeStudent} from '../Interfaces/INoticeStudent'
import UserModel from '../models/User.model'
import LessonsModel from '../models/Lessons.model'
import GradeModel from '../models/Grade.model'
import NoticeTeacherDto from '../dto/Notice.dto/Notice.teacher.dto'

type NoticeAdd = Pick<INotice , 'sender' | 'recipient' | 'model_id' | 'notice_type' >

const noticeController: Router = Router()

noticeController.get('/all' , authMiddleware , async(req:IRequest ,res:Response) => {

    try {
        
        const response = await NoticeModel.findAll({where: {recipient: req.user.id} , include:
            [ 
                {
                    model: UserModel , 
                    as:'from_user' , 
                    attributes:{exclude: ['password', 'token' , 'isConfirmed' ,'phone' , 'email']}
    
                },
                {
                    model: LessonsModel,
                    as: 'lesson',
                },
                {
                    model: GradeModel,
                    as: 'grade',
                    include:[{
                        model:LessonsModel,
                        as:'lesson',

                    }]
                },
    
            ],
            
            
        } )
        

        res.send(response)        

    } catch (error) {
        
        res.status(404).send('FAK')
    }
})

noticeController.get('/teacher' , authMiddleware , async(req:IRequest ,res:Response) => {

    try {
        
        const response = await NoticeModel.findAll({where: {recipient: req.user.id} , include:
            [ 
                {
                    model: UserModel , 
                    as:'from_user' , 
                    attributes:{exclude: ['password', 'token' , 'isConfirmed' ,'phone' , 'email']}
                },
                {
                    model: GradeModel,
                    as: 'grade',
                    include:[{
                        model:LessonsModel,
                        as:'lesson',

                    }]
                },

            ],
            
        } )
        

        res.send(response)        

    } catch (error) {
        
        res.status(404).send('FAK')
    }
})

noticeController.get('/notread' , authMiddleware , async(req:IRequest ,res:Response) => {

    try {
        
        const response = await NoticeModel.findAll({where: {recipient: req.user.id ,isRead: false} , include:
            [ 
                {
                    model: UserModel , 
                    as:'from_user' , 
                    attributes:{exclude: ['password', 'token' , 'isConfirmed' ,'phone' , 'email']}
    
                },
                
            ],
            
        } )

        res.send(response)        

    } catch (error) {
        
        res.status(404).send('FAK')
    }
})

noticeController.post('/student/add' ,authMiddleware,async(req:IRequest ,res:Response) => {

    const body = req.body as Omit<INoticeStudent , 'message' | 'sender' | 'isRead' > 

    try {
        
        const noticeDto: NoticeStudentDto[] = body.recipient.map((val) => {

            return new NoticeStudentDto(req.user.id ,val , body.model_id ,body.notice_type)
        })

        const response = await NoticeModel.bulkCreate(noticeDto as NoticeAdd[])

        res.send(response)
        

    } catch (error) {
        res.status(404).send('FAK')
    }

})

noticeController.post('/teacher/add' ,authMiddleware,async(req:IRequest ,res:Response) => {

    const body = req.body as Pick<INotice ,  'recipient' | 'message'  > 

    try {
        
        const noticeDto : NoticeTeacherDto = new NoticeTeacherDto(req.user.id , body.recipient , body.message)
        
        const response = await NoticeModel.create({...noticeDto})

        res.send(response)
        

    } catch (error) {
        res.status(404).send('FAK')
    }

})

noticeController.put('/read/:id' ,authMiddleware , async(req:IRequest ,res:Response) => {

    const { id } = req.params

    try {
        if(id){
            const response = await NoticeModel.findByPk(id)

            if(response && !response.isRead) {
                response.isRead = true
    
                console.log(response.isRead);
                
                response.save()
    
                return res.send(response)
            } 
        }
        


    } catch (error) {
        res.status(404).send('FAK')   
    }

})
export default noticeController