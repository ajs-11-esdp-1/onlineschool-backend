import {TemplateDto} from "../dto/Template.dto";
import TemplateModel from "../models/Template.model";
import {Router, Request, Response} from "express";

const templateController: Router = Router()

templateController.get('/', async (req: Request, res: Response) => {

    const {id} = req.query

    try {

        if (id) {

            const response = await TemplateModel.findOne({where: {id: id}})

            return res.send(response)

        }

        const response = await TemplateModel.findAll()

        res.send(response)

    } catch (error) {

        res.status(404).send('FAK')

    }

})

templateController.post('/', async (req: Request, res: Response) => {

    const {title, description} = req.body

    try {
        if (title.trim() !== '' && description.trim() !== '') {

            const template: TemplateDto = new TemplateDto(title, description)

            const response = await TemplateModel.create({...template})

            return res.send(response)
        }
        res.status(300).send('поля пустые')
    } catch (error) {
        res.status(404).send('FAK')
    }

})


export default templateController;