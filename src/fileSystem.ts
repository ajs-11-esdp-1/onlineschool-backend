import fs from 'fs'
import path from 'path'


export const PictureTutorialFS = {
    deleteArrPicture : (arrPicture:string[]) => {

        // let pathUploads: string = './public/uploads/tutorials'
        
        
        arrPicture.map(async(val) => {

            const pathUploads:string = path.join('../public/uploads/tutorials', val)

            console.log(pathUploads);
            
            await fs.rm(pathUploads , () => {})
        })
        
    }
}

