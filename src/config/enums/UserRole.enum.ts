export enum UserRole {
  User    = 'user',
  Admin   = 'admin',
  Teacher = 'teacher',
  School  = 'school',
  Student = 'student'
}