import nodemailer from 'nodemailer';
import { NodemailerExpressHandlebarsOptions } from 'nodemailer-express-handlebars';
import path from 'path';
import hbs from 'nodemailer-express-handlebars';
import { IMail } from '../Interfaces/IMail';

const myEmail = 'rais.k-1995@mail.ru';

const sendMail = (data: IMail) => {
  const transporter = nodemailer.createTransport({
    host: 'smtp.mail.ru',
    port: 465,
    secure: true,
    auth: {
      user: myEmail,
      pass: '6p36pKDZgexmGuPjNbtb',
    },
  });
    

  const mailOptions = {
    from: myEmail,
    to: data.recipient,
    subject: data.theme,
    template: 'email',
    context: {
      msg: data.link,
      check:data.check,
    },
  };

  const handlebarOptions: NodemailerExpressHandlebarsOptions = {
    viewEngine: {
      partialsDir: path.resolve('./src/views/'),
      defaultLayout: false,
    },
    viewPath: path.resolve('./src/views/'),
  };

  transporter.use('compile', hbs(handlebarOptions));

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error);
    } else {
      // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
      console.log('Сообщение отправлено: ' + info.response);
    }
    transporter.close();
  });
};


export default sendMail;