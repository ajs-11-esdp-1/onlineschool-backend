export class TemplateDto {
    title:string
    description:string

    constructor(title :string , description:string) {
        this.description = description
        this.title = title
    }   
}