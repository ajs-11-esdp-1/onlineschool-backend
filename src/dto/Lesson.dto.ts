
export default class LessonsDto  {
    title:string
    description :string
    lesson :JSON
    transit_time :string
    template_id:number
    teacher_id:number
    direction_id:number
    pay_order: number | null
    public:boolean
    constructor (title:string,
        description :string,
        lesson :JSON,
        transit_time :string,
        template_id:number,
        teacher_id:number,
        direction_id:number   
        ) {
            this.teacher_id = teacher_id
            this.template_id = template_id
            this.title = title
            this.description = description
            this.lesson = lesson
            this.transit_time = transit_time
            this.direction_id = direction_id
            this.pay_order = null
            this.public = false
    }
}