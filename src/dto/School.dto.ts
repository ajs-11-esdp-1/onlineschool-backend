export default class schoolDto {
  schoolName: string;
  password: string;
  email: string;
  phone: string;
  avatar: string;
  token: string;
  role_id: number;

  constructor (schoolName: string, password: string, role_id: number, phone: string, email: string) {
    this.schoolName = schoolName;
    this.password   = password;
    this.email      = email;
    this.phone      = phone;
    this.avatar     = 'uploads/User.jpeg';
    this.token      = '';
    this.role_id    = role_id;
  }
}