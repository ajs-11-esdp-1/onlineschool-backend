class userAuthDto {
  firstName: string;
  lastName: string;
  password: string;
  email: string;
  phone: string;
  avatar: string;
  token: string;
  role_id: number;
  isConfirmed :boolean

  constructor (firstName: string, lastName: string, password: string, role_id:number, phone: string, email: string, token:string) {
    this.firstName   = firstName;
    this.lastName    = lastName;
    this.password    = password;
    this.email       = email;
    this.phone       = phone;
    this.avatar      = 'uploads/user/user.png';
    this.token       = token;
    this.role_id     = role_id;
    this.isConfirmed = false;
  }
}

export default userAuthDto;