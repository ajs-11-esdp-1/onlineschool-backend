class userChangeDto {
  firstName: string;
  lastName: string;
  phone: string;
  avatar: string;
  
  constructor (firstName: string, lastName: string, phone: string, avatar: string) {
    this.firstName = firstName;
    this.lastName  = lastName;
    this.phone     = phone;
    this.avatar    = avatar;
  }
}
  
export default userChangeDto;