export default class GradeDto {
    lesson_id:number
	grade:string
	transit_time: string
	readed: boolean
	student_id:number
    teacher_id:number
    review: string
    lesson_answer: JSON
    constructor(
        lesson_id:number,
        grade:string,
        transit_time: string,
        student_id:number,
        teacher_id:number,
        lesson_answer: JSON
        ) {
        this.lesson_id = lesson_id
        this.grade = grade
        this.transit_time = transit_time
        this.readed = true
        this.student_id = student_id
        this.teacher_id = teacher_id
        this.review = 'В проверке...'
        this.lesson_answer = lesson_answer
    }
}