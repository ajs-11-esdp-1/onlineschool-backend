import { nanoid } from "nanoid"

class userDto {
      username:string
      password:string
      token:string
      role:string
     
      constructor (username:string,password:string){
            this.password = password
            this.role = 'User'
            this.username = username
            this.token = nanoid()
            
      }     
}

export default userDto;