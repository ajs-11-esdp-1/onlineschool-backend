import { NoticeType } from "@src/config/enums/NoticeType"

export default class NoticeStudentDto {
    sender : number
    recipient: number
    message: string
    model_id: number
    notice_type: NoticeType
    isRead: boolean
    constructor(
        sender : number,
        recipient: number,
        model_id: number,
        notice_type: NoticeType
    ) {
        this.sender = sender
        this.recipient = recipient
        this.message = ''
        this.model_id = model_id
        this.notice_type = notice_type
        this.isRead = false
    }
}