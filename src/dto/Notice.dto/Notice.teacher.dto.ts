import { NoticeType } from "../../config/enums/NoticeType";

export default class NoticeTeacherDto {
    sender : number
    recipient: number
    message: string
    model_id: null
    notice_type: null
    isRead: boolean
    constructor(
        sender : number,
        recipient: number,
        message: string
    ) {
        this.sender = sender
        this.recipient = recipient
        this.message = message
        this.model_id = null
        this.notice_type = null
        this.isRead = false
    }
}

export  class NoticeTeacherByGradeDto {
    sender : number
    recipient: number
    message: string
    model_id: number
    notice_type: NoticeType
    isRead: boolean
    constructor(
        sender : number,
        recipient: number,
        model_id: number
    ) {
        this.sender = sender
        this.recipient = recipient
        this.message = ''
        this.model_id = model_id
        this.notice_type = NoticeType.GRADE
        this.isRead = false
    }
}