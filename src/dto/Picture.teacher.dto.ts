export default class PictureTeacherDto {
    lesson_id: number
    teacher_id: number
    picture: string
    constructor (
        lesson_id: number,
        teacher_id: number,
        picture: string
    ) {
        this.lesson_id = lesson_id
        this.picture = picture 
        this.teacher_id = teacher_id
    }
}