import sequelize from '../config/db.config';
import {nanoid} from "nanoid";
import {DataTypes, Model} from "sequelize";
import bcrypt from "bcrypt";
import generateAccessToken from "@src/helpers/GenerateAccessToken";

const DirectionsModel = sequelize.define<IDirection>('directions', {
    direction:{
        type:DataTypes.STRING,
        allowNull: false
    }
} );

const RoleModel = sequelize.define<IRole>('roles' , {
        role: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    },
    {timestamps: false });


const UserModel = sequelize.define<IUser>('users' , {
        firstName: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        lastName: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique:true,
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        avatar: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        token: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        role_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references : {
                key: 'id',
                model: 'roles',
            },
        },
        isConfirmed: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        },
    },
    {
        timestamps: false,
    });

UserModel.beforeSave(async (user:IUser) => {
    if (user.changed('password')) {
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(user.password, salt);
        user.password = hashedPassword;
    }
});

UserModel.prototype.checkPassword = async function(password :string): Promise<boolean>  {

    const check = await bcrypt.compare(password , this.password);

    return check;
};

UserModel.prototype.toJSON = function () {
    const values = Object.assign({}, this.get());
    delete values.password;
    return values;
};

UserModel.prototype.generateToken = function() {

    this.token = generateAccessToken(this.email);

};

UserModel.prototype.setAvatar = function (avatar: string) {

    this.avatar = avatar;

};

UserModel.belongsTo(RoleModel, {
    foreignKey: 'role_id',
    targetKey: 'id',
});

const TemplateModel = sequelize.define<ITemplate>( 'templates', {
        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    },
    {
        timestamps: false,
    },
);

const LessonsModel = sequelize.define<ILessons>('lessons', {
        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        lesson: {
            type: DataTypes.JSON,
            allowNull: false,
        }
        ,
        transit_time: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        template_id: {
            type: DataTypes.INTEGER,
            allowNull: true,

        },
        teacher_id: {
            type: DataTypes.INTEGER,
            allowNull: false,

        },
        direction_id: {
            type: DataTypes.INTEGER,
            allowNull: false,

        },
        pay_order: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        public: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        }

    },
    {
        timestamps: false,
    },
);

LessonsModel.belongsTo(UserModel, {targetKey: 'id', foreignKey: 'teacher_id'})

LessonsModel.belongsTo(TemplateModel, {targetKey: 'id', foreignKey: 'template_id'})

LessonsModel.belongsTo(DirectionsModel, {targetKey: 'id', foreignKey: 'direction_id'})


export const TeacherStudentModel = sequelize.define<ITeacherStudent>('teacher_student', {
        teacher_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        student_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
    },
    {
        timestamps: false,
    },
);

TeacherStudentModel.belongsTo(UserModel, {as: 'student', targetKey: 'id', foreignKey: 'student_id'});

TeacherStudentModel.belongsTo(UserModel, {as: 'teacher', targetKey: 'id', foreignKey: 'teacher_id'});


export interface IRole extends Model {
    id: number;
    role: string;
}

export interface ITemplate extends Model {
    id:number
    title: string
    description:string
}

export interface IUser extends Model {
    id: number;
    userName: string;
    firstName: string;
    lastName: string;
    password: string;
    email: string;
    phone: string;
    avatar: string;
    token: string;
    role_id: number;
    isConfirmed :boolean
    checkPassword: (password: string) => Promise<boolean>;
    generateToken: () => void;
    setAvatar: (avatar: string) => void;
    toJSON: () => Omit<IUser , 'password'>;
}

export interface ILessons extends Model {
    id: number
    title: string
    description:string
    lesson:JSON
    transit_time:string
    template_id:number | null
    teacher_id:number
    direction_id:number
    pay_order: number | null
    public:boolean
}

export interface IDirection extends Model {
    id :number
    direction :string
}

export interface ITeacherStudent extends Model {
    teacher_id:number
    student_id:number
}


const run = async () => {

    const lessonWords = "{\"arrWords\":[{\"value\":\"Программирование\",\"id\":\"RrCIz_R_QS4gnM_1UfSGq\",\"isDrable\":false,\"styleHandler\":false},{\"value\":\"—\",\"id\":\"utesjoC-e2MBO0OHDkoWs\",\"isDrable\":false,\"styleHandler\":false},{\"value\":\"это\",\"id\":\"7feznkPAjtfsH9nwbOGIo\",\"isDrable\":true,\"styleHandler\":true},{\"value\":\"искусство\",\"id\":\"d6LZceACHwdgP84J09kcL\",\"isDrable\":false,\"styleHandler\":false},{\"value\":\"создания\",\"id\":\"42Il_b9A6kXb_7t0nUP6E\",\"isDrable\":false,\"styleHandler\":false},{\"value\":\"новых\",\"id\":\"nddxcBK0VImhHJdf5sZdO\",\"isDrable\":true,\"styleHandler\":true},{\"value\":\"миров,\",\"id\":\"Z6lGOBZk9fhFHjP5vkYiL\",\"isDrable\":false,\"styleHandler\":false},{\"value\":\"где\",\"id\":\"Swf_CwTGUoKkbtiCwt0uO\",\"isDrable\":false,\"styleHandler\":false},{\"value\":\"ограничивает\",\"id\":\"ZHM5NnVTXGGjS0qxMTplT\",\"isDrable\":true,\"styleHandler\":true},{\"value\":\"лишь\",\"id\":\"oYiGK09ySGNH-mXhr7awf\",\"isDrable\":false,\"styleHandler\":false},{\"value\":\"ваша\",\"id\":\"OrjC6deUskB-GsbmKWY-v\",\"isDrable\":false,\"styleHandler\":false},{\"value\":\"фантазия,\",\"id\":\"DYKOhr7q7akbLqdJb1PkL\",\"isDrable\":false,\"styleHandler\":false},{\"value\":\"а\",\"id\":\"TMJR6QHoghqKij_hPMXMv\",\"isDrable\":false,\"styleHandler\":false},{\"value\":\"результаты\",\"id\":\"_RlAdIg29MimMujJVLaqS\",\"isDrable\":true,\"styleHandler\":true},{\"value\":\"приносят\",\"id\":\"-kD6IEqnrzrAetCgNFDg1\",\"isDrable\":false,\"styleHandler\":false},{\"value\":\"пользу\",\"id\":\"Fqz8FSaQVRJAs_aaP7ZZa\",\"isDrable\":false,\"styleHandler\":false},{\"value\":\"всему\",\"id\":\"3e8jIufDM84hjs23prv_f\",\"isDrable\":true,\"styleHandler\":true},{\"value\":\"человечеству.\",\"id\":\"t7JVCfQaacGRhoVLQLQ3l\",\"isDrable\":false,\"styleHandler\":false}],\"checkedWord\":[{\"value\":\"новых\",\"id\":\"nddxcBK0VImhHJdf5sZdO\",\"isDrable\":true,\"styleHandler\":true},{\"value\":\"ограничивает\",\"id\":\"ZHM5NnVTXGGjS0qxMTplT\",\"isDrable\":true,\"styleHandler\":true},{\"value\":\"результаты\",\"id\":\"_RlAdIg29MimMujJVLaqS\",\"isDrable\":true,\"styleHandler\":true},{\"value\":\"всему\",\"id\":\"3e8jIufDM84hjs23prv_f\",\"isDrable\":true,\"styleHandler\":true},{\"value\":\"это\",\"id\":\"7feznkPAjtfsH9nwbOGIo\",\"isDrable\":true,\"styleHandler\":true}]}"

    const lessonsQuotes = "{\"theme\":[{\"id\":\"19lWLK_DsySLLvtIYevsK\",\"theme\":\"Рыба\"},{\"id\":\"28JWuH96BVYiL-MOaK33H\",\"theme\":\"Ягоды\"}],\"arrPicture\":[{\"id\":\"JdQ7YHEDJyxe-Di4n7rla\",\"theme_id\":\"19lWLK_DsySLLvtIYevsK\",\"picture\":\"zcdUHXBjOO-WeHqr9h_pU.png\",\"dragbel\":true},{\"id\":\"V8gVV4awXh_KUvHbBt9s2\",\"theme_id\":\"28JWuH96BVYiL-MOaK33H\",\"picture\":\"pEw-GE7Yw9Y0mTbWHuW4X.png\",\"dragbel\":true}]}"

    const quizLesson = "{\"quiz\":[{\"question\":\"cat\",\"options\":[{\"option\":\"кошка\"},{\"option\":\"собака\"},{\"option\":\"руль\"},{\"option\":\"стол\"}],\"correctAnswer\":\"кошка\"},{\"question\":\"dog\",\"options\":[{\"option\":\"собака\"},{\"option\":\"мясо\"},{\"option\":\"шляпа\"},{\"option\":\"дом\"}],\"correctAnswer\":\"собака\"},{\"question\":\"house\",\"options\":[{\"option\":\"дом\"},{\"option\":\"стол\"},{\"option\":\"небо\"},{\"option\":\"друг\"}],\"correctAnswer\":\"дом\"}]}";

    const writtenAnswerFixture = "{\"questions\": [{\"question\": \"Для проверки вопроса напиши \\\"answer\\\" в поле ниже\",\"answers\": [{\"answer\": \"answer\"}],\"picture\": \"4FEG9lTwxTQMVCzlwHb5W.png\"},{\"answers\": [{\"answer\": \"test\"}],\"question\": \"Для проверки вопроса впиши \\\"test\\\" в поле ниже\", \"picture\":null}],\"arrPictures\": [{\"id\": \"yspdwefBB1V4acHR-SrEQ\",\"question_id\": 0,\"picture\": \"4FEG9lTwxTQMVCzlwHb5W.png\"}]}";

    const mahjongLesson = "{\"images\":[{\"id\":\"NhOZQblvyAln\",\"value\":\"IF9ttKo_zIT7Az67tqQ_o.png\",\"isMatched\":false,\"isVisible\":false},{\"id\":\"pJXJZSMhmwrt\",\"value\":\"vQ3BM2PpeJy_ywyaAQEZK.png\",\"isMatched\":false,\"isVisible\":false},{\"id\":\"TbQOTmPDoXAZ\",\"value\":\"nenm0vbreq0lC-x0VwKg9.png\",\"isMatched\":false,\"isVisible\":false},{\"id\":\"zLLfnYwoGKNw\",\"value\":\"WjE1koNoAshzpitArceTP.png\",\"isMatched\":false,\"isVisible\":false},{\"id\":\"zaQnJChvJzfj\",\"value\":\"62wq8TMEBCt3Sy4pbf6_3.png\",\"isMatched\":false,\"isVisible\":false},{\"id\":\"xBbGUIftvvxN\",\"value\":\"DZwhAnEn2zXlviXbYadRy.png\",\"isMatched\":false,\"isVisible\":false},{\"id\":\"ubiMcyJhdqHh\",\"value\":\"vEMlFQ4aD1LuUTYbve44w.png\",\"isMatched\":false,\"isVisible\":false},{\"id\":\"hHpJRZCVxBKS\",\"value\":\"CbKBPwlSyOXY8b8hOgfCK.png\",\"isMatched\":false,\"isVisible\":false}]}";

    const rolesArr: Pick<IRole,
        'role'>[] = [
        {role: 'student'},
        {role: 'teacher'},
        {role: 'school'}];

    const arrTemplate: Pick<ITemplate,
        'title' |
        'description'>[] = [
        {title: 'Распредели правильно слова', description: 'Распредели правильно'},
        {title: 'Распредели правильно картинки', description: 'Распредели правильно'},
        {title: 'Впиши ответ', description: 'Впиши ответ'},
        {title: 'Маджонг', description: 'Найди одинаковые картинки'},
        {title: 'Сопоставление картинок по словам', description: 'Сопоставление'},
        {title: 'Викторина 1 из 4', description: 'Викторина'},
    ];

    const arrDirections: Pick<IDirection, 'direction'>[] = [{direction: 'Aнглийский'}, {direction: 'Математика'}];

    const arrLessons: Pick<ILessons,
        'teacher_id' |
        'title' |
        'template_id' |
        'description' |
        'transit_time' |
        'lesson' |
        'direction_id' |
        'pay_order' |
        'public'
    >[] = [
        {
            title: 'Собери правильно предложение',
            description: 'Найди правильные места для слов',
            transit_time: '600',
            teacher_id: 2,
            lesson: JSON.parse(JSON.stringify(lessonWords)),
            template_id: 1,
            pay_order: null,
            public: true,
            direction_id: 1,
        },
        {
            title: 'Распредели правильно картинки',
            description: 'Найди фрукты и овощи',
            transit_time: '600',
            teacher_id: 2,
            lesson: JSON.parse(JSON.stringify(lessonsQuotes)),
            template_id: 2,
            pay_order: null,
            public: true,
            direction_id: 1,
        },
        {
            title: 'Задание по познанию мира',
            description: 'Впиши правильные ответы',
            transit_time: '600',
            teacher_id: 2,
            lesson: JSON.parse(JSON.stringify(writtenAnswerFixture)),
            template_id: 3,
            pay_order: null,
            public: true,
            direction_id: 1,
        },
        {
            title: 'Поиск пары из картинок',
            description: 'Найди одинаковые картинки',
            transit_time: '600',
            teacher_id: 2,
            lesson: JSON.parse(JSON.stringify(mahjongLesson)),
            template_id: 4,
            pay_order: null,
            public: true,
            direction_id: 1,
        },
        {
            title: 'Викторина по английскому языку',
            description: 'Выбери правильный ответ',
            transit_time: '600',
            teacher_id: 2,
            lesson: JSON.parse(JSON.stringify(quizLesson)),
            template_id: 6,
            pay_order: null,
            public: true,
            direction_id: 1,
        },
    ];

    const usersArr: Pick<IUser,
        'firstName' |
        'lastName' |
        'password' |
        'email' |
        'avatar' |
        'token' |
        'role_id' |
        'isConfirmed' |
        'phone'
    >[] = [
        {
            firstName: 'John',
            lastName: 'Doe',
            password: '$2b$10$JLrhwGnXvSX9qXJZHyL0K.H2HG6IUNqzlPlYEwV.J19nWzpzQAiRS',
            email: 'jhon@mail.ru',
            phone: '3301123131',
            avatar: 'user.png',
            token: nanoid(),
            role_id: 3,
            isConfirmed: true,
        },
        {
            firstName: 'Елена',
            lastName: 'Петрова',
            password: '$2b$10$JLrhwGnXvSX9qXJZHyL0K.H2HG6IUNqzlPlYEwV.J19nWzpzQAiRS',
            email: 'ser@mail.ru',
            phone: '3301',
            avatar: 'user.png',
            token: nanoid(),
            role_id: 2,
            isConfirmed: true,
        },
        {
            firstName: 'София',
            lastName: 'Некрасова',
            password: '$2b$10$JLrhwGnXvSX9qXJZHyL0K.H2HG6IUNqzlPlYEwV.J19nWzpzQAiRS',
            email: 'gi@mail.ru',
            phone: '3301',
            avatar: 'user.png',
            token: nanoid(),
            role_id: 1,
            isConfirmed: true,
        },
    ];

    const studentTeacher: Pick<ITeacherStudent, 'teacher_id' | 'student_id'> [] = [
        {
            teacher_id: 2,
            student_id: 3
        }
    ];

    try {

        await sequelize.authenticate();

        await sequelize.sync({force: true});

        await RoleModel.bulkCreate(rolesArr);

        await TemplateModel.bulkCreate(arrTemplate);

        await UserModel.bulkCreate(usersArr);

        await DirectionsModel.bulkCreate(arrDirections);

        await LessonsModel.bulkCreate(arrLessons);

        await TeacherStudentModel.bulkCreate(studentTeacher);

    } catch (error) {

        console.log(error);
    }
};

run();