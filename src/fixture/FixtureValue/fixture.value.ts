/*
import { IDirection } from '../../Interfaces/IDirection';
import { IRole } from '../../Interfaces/IRole';
import { ITemplate } from '../../Interfaces/ITemplate';
import { IUser } from '../../Interfaces/IUser';
import { nanoid } from 'nanoid';
import { ILessons } from '../../Interfaces/ILessons';
import { lessonsQuotes, lessonWords, writtenAnswerFixture, mahjongLesson, quizLesson } from './lessons.value';
import { ITeacherStudent } from '../../Interfaces/TeacherStudent';


export const rolesArr: Pick<IRole,
  'role'>[] = [
    { role: 'student' },
    { role: 'teacher' },
    { role: 'school' }];

export const arrTemplate: Pick<ITemplate,
  'title' |
  'description'>[] = [
    { title: 'Распредели правильно слова', description: 'Распредели правильно'},
    { title: 'Распредели правильно картинки', description: 'Распредели правильно'},
    { title: 'Впиши ответ', description: 'Впиши ответ'},
    { title: 'Маджонг', description: 'Найди одинаковые картинки'},
    { title: 'Сопоставление картинок по словам', description: 'Сопоставление'},
    { title: 'Викторина 1 из 4', description: 'Викторина'},
  ];

export const arrDirections: Pick<IDirection, 'direction'>[] = [{direction: 'Aнглийский'}, {direction: 'Математика'}];

export const arrLessons: Pick<ILessons,
  'teacher_id' |
  'title' |
  'template_id' |
  'description' |
  'transit_time' |
  'lesson' |
  'direction_id' |
  'pay_order' |
  'public'
>[] = [
  {
    title: 'Собери правильно предложение',
    description: 'Найди правильные места для слов',
    transit_time: '600',
    teacher_id: 2,
    lesson: JSON.parse(JSON.stringify(lessonWords)),
    template_id: 1,
    pay_order: null,
    public: true,
    direction_id: 1,
  },
  {
    title: 'Распредели правильно картинки',
    description: 'Найди фрукты и овощи',
    transit_time: '600',
    teacher_id: 2,
    lesson: JSON.parse(JSON.stringify(lessonsQuotes)),
    template_id: 2,
    pay_order: null,
    public: true,
    direction_id: 1,
  },
  {
    title: 'Задание по познанию мира',
    description: 'Впиши правильные ответы',
    transit_time: '600',
    teacher_id: 2,
    lesson: JSON.parse(JSON.stringify(writtenAnswerFixture)),
    template_id: 3,
    pay_order: null,
    public: true,
    direction_id: 1,
  },
  {
    title: 'Поиск пары из картинок',
    description: 'Найди одинаковые картинки',
    transit_time: '600',
    teacher_id: 2,
    lesson: JSON.parse(JSON.stringify(mahjongLesson)),
    template_id: 4,
    pay_order: null,
    public: true,
    direction_id: 1,
  },
  {
    title:'Викторина по английскому языку',
    description: 'Выбери правильный ответ',
    transit_time: '600',
    teacher_id: 2,
    lesson: JSON.parse(JSON.stringify(quizLesson)),
    template_id: 6,
    pay_order: null,
    public: true,
    direction_id: 1,
  },
];

export const usersArr: Pick<IUser,
  'firstName' |
  'lastName' |
  'password' |
  'email' |
  'avatar' |
  'token' |
  'role_id' |
  'isConfirmed' |
  'phone'
>[] = [
  {
    firstName: 'John',
    lastName: 'Doe',
    password: '$2b$10$JLrhwGnXvSX9qXJZHyL0K.H2HG6IUNqzlPlYEwV.J19nWzpzQAiRS',
    email: 'jhon@mail.ru',
    phone: '3301123131',
    avatar: 'user.png',
    token: nanoid(),
    role_id: 3,
    isConfirmed: true,
  },
  {
    firstName: 'Елена',
    lastName: 'Петрова',
    password: '$2b$10$JLrhwGnXvSX9qXJZHyL0K.H2HG6IUNqzlPlYEwV.J19nWzpzQAiRS',
    email: 'ser@mail.ru',
    phone: '3301',
    avatar: 'user.png',
    token: nanoid(),
    role_id: 2,
    isConfirmed: true,
  },
  {
    firstName: 'София',
    lastName: 'Некрасова',
    password: '$2b$10$JLrhwGnXvSX9qXJZHyL0K.H2HG6IUNqzlPlYEwV.J19nWzpzQAiRS',
    email: 'gi@mail.ru',
    phone: '3301',
    avatar: 'user.png',
    token: nanoid(),
    role_id: 1,
    isConfirmed: true,
  },
];


export const studentTeacher:Pick<ITeacherStudent , 'teacher_id' | 'student_id'> [] = [
  {
    teacher_id: 2,
    student_id: 3
  }
]*/
