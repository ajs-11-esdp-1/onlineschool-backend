import multer from 'multer';
import {uploadPath} from '../config/config';
import {nanoid} from 'nanoid';
import path from 'path';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, uploadPath);
  },
  filename(req, file, cb) {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const uploadMiddleware = multer({storage});

export default uploadMiddleware;