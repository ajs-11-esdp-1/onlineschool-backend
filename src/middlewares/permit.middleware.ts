import { IRequest } from '../Interfaces/IRequest';
import { NextFunction, Response } from 'express';
import { UserRole } from '../helpers/enums/UserRole.enum';

export const permitMiddleware = (role: UserRole) =>
  (req: IRequest, res: Response, next: NextFunction) => {
    if (!req.user) {
      res.sendStatus(401);
      return;
    }
    if (req.user.role.role !== role) {
      return res.sendStatus(403);
    }
    next();
};