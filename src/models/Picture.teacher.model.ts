import { IPictureTeacher } from '@src/Interfaces/PictureTeacher';
import sequelize from '../config/db.config';
import { DataTypes } from 'sequelize';
import UserModel from './User.model';
import LessonsModel from './Lessons.model';

const PictureTeacherModel = sequelize.define<IPictureTeacher>( 'picture_teacher' , {

    lesson_id :{
        type: DataTypes.INTEGER,
        allowNull: false
    },
    teacher_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    picture: {
        type: DataTypes.STRING,
        allowNull: false
    }

})



PictureTeacherModel.belongsTo(UserModel , {as: 'teacher' ,targetKey: 'id' , foreignKey: 'teacher_id' })

PictureTeacherModel.belongsTo(LessonsModel , {as: 'lesson' ,targetKey: 'id' , foreignKey: 'lesson_id'})


export default PictureTeacherModel