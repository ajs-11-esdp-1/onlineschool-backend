import { DataTypes } from 'sequelize';
import sequelize from '../config/db.config';
import bcrypt from 'bcrypt';
import generateAccessToken from '../helpers/GenerateAccessToken';
import {ISchool} from '../Interfaces/ISchool';
import RoleModel from './Role.model';

const SchoolModel = sequelize.define<ISchool>('schools' , {
  schoolName: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  phone: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  avatar: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  token: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  role_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    references : {
      key: 'id',
      model: 'roles',
    },
  },
});

SchoolModel.beforeSave(async (school:ISchool) => {

  if (school.changed('password')) {
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(school.password, salt);
    school.password = hashedPassword;
  }
});

SchoolModel.prototype.checkPassword = async function (password :string)  {
  return bcrypt.compare(password, this.password);
};

SchoolModel.prototype.toJSON = function () {
  const values = Object.assign({}, this.get());
  delete values.password;
  return values;
};

SchoolModel.prototype.generateToken = function() {

  this.token = generateAccessToken(this.email);

};

SchoolModel.prototype.setAvatar = function (avatar: string) {

  this.avatar = avatar;

};

SchoolModel.belongsTo(RoleModel, {
  foreignKey: 'role_id',
  targetKey: 'id',
});

export default SchoolModel;