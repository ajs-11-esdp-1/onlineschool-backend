import sequelize from '../config/db.config';
import {DataTypes} from 'sequelize';
import UserModel from './User.model';
import TemplateModel from './Template.model';
import {ILessons} from '../Interfaces/ILessons';
import DirectionsModel from './Directions.model';


const LessonsModel = sequelize.define<ILessons>('lessons', {
        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        lesson: {
            type: DataTypes.JSON,
            allowNull: false,
        }
        ,
        transit_time: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        template_id: {
            type: DataTypes.INTEGER,
            allowNull: true,

        },
        teacher_id: {
            type: DataTypes.INTEGER,
            allowNull: false,

        },
        direction_id: {
            type: DataTypes.INTEGER,
            allowNull: false,

        },
        pay_order: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        public: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        }

    },
    {
        timestamps: false,
    },
);

LessonsModel.belongsTo(UserModel, {targetKey: 'id', foreignKey: 'teacher_id'})

LessonsModel.belongsTo(TemplateModel, {targetKey: 'id', foreignKey: 'template_id'})

LessonsModel.belongsTo(DirectionsModel, {targetKey: 'id', foreignKey: 'direction_id'})

export default LessonsModel;