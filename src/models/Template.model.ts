import sequelize from '../config/db.config';
import { DataTypes } from 'sequelize';
import { ITemplate } from '../Interfaces/ITemplate';


const TemplateModel = sequelize.define<ITemplate>( 'templates', {
  title: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.STRING,
    allowNull: false,
  },
},
{
  timestamps: false,
},
);

export default TemplateModel;