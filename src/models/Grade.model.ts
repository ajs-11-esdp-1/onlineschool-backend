import { DataTypes } from 'sequelize';
import sequelize from '../config/db.config';
import IGrade from '../Interfaces/IGrade';
import UserModel from './User.model';
import LessonsModel from './Lessons.model';

const GradeModel = sequelize.define<IGrade>('grades' , {
    lesson_id: {
        type: DataTypes.INTEGER,
        allowNull:false
    },
    grade: {
        type: DataTypes.STRING,
        allowNull:false
    },
    transit_time: {
        type: DataTypes.STRING,
        allowNull:false
    },
    readed: {
        type: DataTypes.BOOLEAN,
        allowNull:false
    },
    student_id: {
        type: DataTypes.INTEGER,
        allowNull:false,
    },
    lesson_answer: {
        type: DataTypes.JSON,
        allowNull:false,
    },
    teacher_id: {
        type: DataTypes.INTEGER,
        allowNull:false,
    },
    review: {
        type: DataTypes.STRING,
        allowNull:true,
    }
  }
   
);
  
GradeModel.belongsTo(UserModel , {as: 'student' ,targetKey: 'id' , foreignKey: 'teacher_id' })

GradeModel.belongsTo(UserModel , {as: 'teacher' ,targetKey: 'id' , foreignKey: 'student_id'})

GradeModel.belongsTo(LessonsModel , {as :'lesson',targetKey: 'id' , foreignKey: 'lesson_id'})


export default GradeModel;