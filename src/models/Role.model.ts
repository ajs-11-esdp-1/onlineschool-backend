import { DataTypes } from 'sequelize';
import sequelize from '../config/db.config';
import {IRole} from '../Interfaces/IRole';

const RoleModel = sequelize.define<IRole>('roles' , {
  role: {
    type: DataTypes.STRING,
    allowNull: false,
  },
},
{timestamps: false });

export default RoleModel;