import sequelize from '../config/db.config';
import { NoticeType } from '../config/enums/NoticeType';
import { DataTypes } from 'sequelize';
import UserModel from './User.model';
import LessonsModel from './Lessons.model';
import GradeModel from './Grade.model';
import INotice from '../Interfaces/INoticeStudent';


export const NoticeModel = sequelize.define<INotice>('notice_student', {

  sender :{
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  recipient:{
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  model_id: {
    type: DataTypes.INTEGER,
    allowNull: true,
  },
  notice_type: {
    type: DataTypes.ENUM(NoticeType.LESSON , NoticeType.GRADE),
    allowNull:true,
  },
  message: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  isRead: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
  },

},
{
  timestamps: false, 
},
);

NoticeModel.belongsTo(UserModel , {as: 'from_user' ,targetKey: 'id' , foreignKey: 'sender' });

NoticeModel.belongsTo(UserModel , {as: 'to_user' ,targetKey: 'id' , foreignKey: 'recipient'});

NoticeModel.belongsTo(LessonsModel , {as: 'lesson' ,targetKey: 'id' , foreignKey: 'model_id'});

NoticeModel.belongsTo(GradeModel , {as: 'grade' ,targetKey: 'id' , foreignKey: 'model_id'});


