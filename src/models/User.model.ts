import { DataTypes } from 'sequelize';
import sequelize from '../config/db.config';
import bcrypt from 'bcrypt';
import {IUser} from '../Interfaces/IUser';
import generateAccessToken from '../helpers/GenerateAccessToken';
import RoleModel from './Role.model';

const UserModel = sequelize.define<IUser>('users' , {
  firstName: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  lastName: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    unique:true,
  },
  phone: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  avatar: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  token: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  role_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    references : {
      key: 'id',
      model: 'roles',
    },
  },
  isConfirmed: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
  },
},
{
  timestamps: false,
});

UserModel.beforeSave(async (user:IUser) => {
  if (user.changed('password')) {
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(user.password, salt);
    user.password = hashedPassword;
  }
});

UserModel.prototype.checkPassword = async function(password :string): Promise<boolean>  {

  const check = await bcrypt.compare(password , this.password);

  return check;
};

UserModel.prototype.toJSON = function () {
  const values = Object.assign({}, this.get());
  delete values.password;
  return values;
};

UserModel.prototype.generateToken = function() {

  this.token = generateAccessToken(this.email);

};

UserModel.prototype.setAvatar = function (avatar: string) {

  this.avatar = avatar;

};

UserModel.belongsTo(RoleModel, {
  foreignKey: 'role_id',
  targetKey: 'id',
});

export default UserModel;