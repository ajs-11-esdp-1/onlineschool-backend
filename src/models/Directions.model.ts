import sequelize from "../config/db.config";
import { IDirection } from "../Interfaces/IDirection";
import { DataTypes } from 'sequelize';


const DirectionsModel = sequelize.define<IDirection>('directions', {
    direction:{
        type:DataTypes.STRING,
        allowNull: false
    }
} )

export default DirectionsModel