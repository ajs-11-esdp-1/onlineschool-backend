import {ITeacherStudent} from '../Interfaces/TeacherStudent';
import sequelize from '../config/db.config';
import {DataTypes} from 'sequelize';
import UserModel from './User.model';


export const TeacherStudentModel = sequelize.define<ITeacherStudent>('teacher_student', {
        teacher_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        student_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
    },
    {
        timestamps: false,
    },
);

TeacherStudentModel.belongsTo(UserModel, {as: 'student', targetKey: 'id', foreignKey: 'student_id'});

TeacherStudentModel.belongsTo(UserModel, {as: 'teacher', targetKey: 'id', foreignKey: 'teacher_id'});